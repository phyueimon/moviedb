import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_movie/movie/list_page/movie_list_page.dart';
import 'package:flutter_movie/movie/movie_bloc/movie_bloc.dart';
import 'package:flutter_movie/repository/movie_repository.dart';
import 'package:flutter_movie/util/colors.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Movie App',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: primaryBlack,
        accentColor: Colors.red,
        textTheme: TextTheme(bodyText2: TextStyle(color: Colors.white)),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MultiBlocProvider(providers: [
        BlocProvider(
          create: (context) =>
              MovieBloc(repository: MovieRepository())..add(InitFetch()),
        ),
      ], child: MovieListPage()),
    );
  }
}
