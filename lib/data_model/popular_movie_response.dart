import 'package:flutter_movie/data_model/movie_data.dart';

class PopularMovieResponse{
    final int page;
    final int totalPages;
    final List<MovieData> results;
    final int responseStatusCode;

    const PopularMovieResponse({
      this.page,
      this.totalPages,
      this.results,
      this.responseStatusCode
    });

    static PopularMovieResponse fromJson(dynamic json,int statusCode){
      if(statusCode == 200){
        var dataList = json['results'] as List;
        return PopularMovieResponse(
          page: json['page'],
          totalPages: json['total_pages'],
          results: dataList.map((data) {
            return MovieData.fromJson(data);
          }).toList(),
          responseStatusCode :statusCode,
        );
      }else{
        return PopularMovieResponse(responseStatusCode: statusCode);
      }
    }
}