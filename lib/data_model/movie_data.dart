import 'package:equatable/equatable.dart';

class MovieData extends Equatable {
  final int id;
  final String originalTitle;
  final String originalLanguage;
  final String posterPath;
  final String releaseDate;
  final double popularity;
  final double voteAverage;
  final String overview;

  const MovieData({
    this.id,
    this.originalTitle,
    this.originalLanguage,
    this.posterPath,
    this.releaseDate,
    this.popularity,
    this.voteAverage,
    this.overview
  });

  static MovieData fromJson(dynamic json) {
    return MovieData(
      id: json['id'],
      originalTitle: json['original_title'],
      originalLanguage: json['original_language'],
      posterPath: json['poster_path'],
      releaseDate: json['release_date'],
      popularity:  double.parse(json['popularity'].toString()),
      voteAverage:  double.parse(json['vote_average'].toString()),
      overview: json['overview'],
    );
  }

  @override
  List<Object> get props =>
      [
        id,
        originalTitle,
        posterPath,
        releaseDate,
        popularity,
        voteAverage,
        overview,
      ];
}