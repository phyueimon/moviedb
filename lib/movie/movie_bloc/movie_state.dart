part of 'movie_bloc.dart';

abstract class MovieState extends Equatable {
  const MovieState();

  @override
  List<Object> get props => [];
}

class Loading extends MovieState {}

class Loaded extends MovieState {
  final List<MovieData> items;
  final int totalPage;
  final int curPage;

  const Loaded({@required this.items,@required this.totalPage,@required this.curPage});

  @override
  List<Object> get props => [items,totalPage,curPage];
}

class Failure extends MovieState {}
