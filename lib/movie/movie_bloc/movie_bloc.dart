import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_movie/data_model/movie_data.dart';
import 'package:flutter_movie/repository/movie_repository.dart';

part 'movie_event.dart';
part 'movie_state.dart';

class MovieBloc extends Bloc<MovieEvent, MovieState> {
  final MovieRepository repository;

  MovieBloc({@required this.repository}) : super(Loading());

  @override
  Stream<MovieState> mapEventToState(MovieEvent event) async* {
    if (event is InitFetch) {
      yield Loading();
      final movieResult = await repository.fetchPopularMovie(1);
      if(movieResult.responseStatusCode == 200){
        yield Loaded(items: movieResult.results,totalPage: movieResult.totalPages,curPage: movieResult.page);
      }else{
        yield Failure();
      }
    }else if (event is LoadMore) {
      final currentState = state;
      if(currentState is Loaded){
        if(currentState.curPage < currentState.totalPage){
          final movieResult = await repository.fetchPopularMovie(currentState.curPage);
          if(movieResult.responseStatusCode == 200){
            yield Loaded(items: currentState.items + movieResult.results,totalPage: movieResult.totalPages,curPage: movieResult.page);
          }
        }
      }
    }
  }
}
