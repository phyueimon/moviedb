import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_movie/data_model/movie_data.dart';
import 'package:flutter_movie/movie/detail_page/movie_detail_page.dart';
import 'package:flutter_movie/movie/list_page/views/movie_item_view.dart';
import 'package:flutter_movie/movie/movie_bloc/movie_bloc.dart';
import 'package:flutter_movie/util/colors.dart';
import 'package:flutter_movie/util/values.dart';
import 'package:flutter_movie/util/widget_general/list_bottom_loader.dart';

class MovieListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Movie DB"),
      ),
      backgroundColor: Color(blackPrimaryValue),
      body: MovieListStateful(),
    );
  }
}

class MovieListStateful extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MovieListState();
  }
}

class MovieListState extends State<MovieListStateful> {
  final _scrollController = ScrollController();
  final _scrollThreshold = 100.0;
  MovieBloc _movieBloc;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
    _movieBloc = BlocProvider.of<MovieBloc>(context);
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {
      _movieBloc.add(LoadMore());
    }
  }

  void _goToDetail(MovieData movieData) {
    MovieDetailPage.goToMovieDetail(context, movieData);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MovieBloc, MovieState>(builder: (context, state) {
      if (state is Failure) {
        return GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            _movieBloc.add(InitFetch());
          },
          child: Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.refresh,
                size: largeIconSize,
                color: Colors.red,
              ),
              SizedBox(height: largePadding),
              Text("Can't load data."),
            ],
          )),
        );
      }
      if (state is Loaded) {
        if (state.items.isEmpty) {
          return Center(
            child: Text("no movie."),
          );
        }
        return ListView.builder(
          itemCount: state.curPage < state.totalPage
              ? state.items.length + 1
              : state.items.length,
          itemBuilder: (BuildContext context, int index) {
            return state.items.length == index
                ? ListBottomLoader()
                : MovieItemView(state.items[index], _goToDetail);
          },
          controller: _scrollController,
        );
      }
      return Center(
        child: CircularProgressIndicator(),
      );
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}
