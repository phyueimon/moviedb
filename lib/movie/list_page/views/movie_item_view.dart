import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_movie/data_model/movie_data.dart';
import 'package:flutter_movie/util/constants.dart';
import 'package:flutter_movie/util/values.dart';

class MovieItemView extends StatelessWidget{
  final MovieData movieData;
  final Function _goToDetail;

  MovieItemView(this.movieData,this._goToDetail);

  @override
  Widget build(BuildContext context) {
   return GestureDetector(
     behavior: HitTestBehavior.translucent,
     onTap: (){
        _goToDetail(movieData);
     },
     child:Padding(
         padding: EdgeInsets.all(smallPadding),
         child : Row(
           crossAxisAlignment: CrossAxisAlignment.start,
           children: <Widget>[
             Container(
               width: 150,
               height: 200,
               child: CachedNetworkImage(
                 imageUrl: "$img_url${movieData.posterPath}",
                 fit: BoxFit.cover,
               ),
             ),
             Expanded(
               child: Padding(
                 padding: EdgeInsets.only(left: largePadding),
                 child: Column(
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: <Widget>[
                     Text(movieData.originalTitle,style: TextStyle(fontSize: largeFontSize,fontWeight: FontWeight.bold,), maxLines: 2,overflow: TextOverflow.ellipsis),
                     SizedBox(height: smallPadding),
                     Text("${movieData.releaseDate}"),
                     SizedBox(height: smallPadding),
                     Text("Language: ${movieData.originalLanguage}",style:TextStyle(color: Colors.grey),maxLines: 2,overflow: TextOverflow.ellipsis),
                     SizedBox(height: smallPadding),
                     Row(
                       children: <Widget>[
                         Icon(Icons.star,color: Colors.redAccent,),
                         SizedBox(height: xSmallPadding),
                         Text(movieData.voteAverage.toString(),style: TextStyle(fontSize: largeFontSize)),
                         Text(" / 10")
                       ],
                     ),
                   ],
                 ),
               ),
             )
           ],
         )
     )
   );
  }
}