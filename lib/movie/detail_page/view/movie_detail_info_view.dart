import 'package:flutter/material.dart';
import 'package:flutter_movie/data_model/movie_data.dart';
import 'package:flutter_movie/util/values.dart';

class MovieDetailInfoView extends StatelessWidget{
  final MovieData movieData;

  MovieDetailInfoView(this.movieData);

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(mediumPadding),
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              children: [
                Icon(Icons.star, color: Colors.redAccent),
                SizedBox(height: xSmallPadding),
                Row(
                  children: [
                    Text(movieData.voteAverage.toString(),
                        style: TextStyle(fontSize: 20)),
                    Text(" / 10")
                  ],
                )
              ],
            ),
            Column(
              children: [
                Text(
                  movieData.popularity.toString(),
                  style: TextStyle(color: Colors.green, fontSize: largeFontSize),
                ),
                SizedBox(height: xSmallPadding),
                Text("Popularity")
              ],
            ),
          ],
        ),
        Divider(color: Colors.grey[800]),
        SizedBox(height: smallPadding),
        Text("Overview",
            style:
            TextStyle(fontSize: xlargeFontSize)),
        Padding(
          padding: EdgeInsets.only(top: largePadding),
          child: Text(movieData.overview,style: TextStyle(height: 1.5)),
        )
      ],
    );
  }

}