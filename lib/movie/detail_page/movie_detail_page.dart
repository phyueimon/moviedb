import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_movie/data_model/movie_data.dart';
import 'package:flutter_movie/movie/detail_page/view/movie_detail_info_view.dart';
import 'package:flutter_movie/util/colors.dart';
import 'package:flutter_movie/util/constants.dart';

class MovieDetailPage extends StatelessWidget {
  final MovieData movieData;

  static void goToMovieDetail(BuildContext context, MovieData movieData) {
    Navigator.push(
      context,
      new MaterialPageRoute(
          builder: (context) => new MovieDetailPage(movieData)),
    );
  }

  MovieDetailPage(this.movieData);

  @override
  Widget build(BuildContext context) {
    return MovieDetailStateful(movieData);
  }
}

class MovieDetailStateful extends StatefulWidget {
  final MovieData movieData;

  MovieDetailStateful(this.movieData);

  @override
  State<StatefulWidget> createState() {
    return MovieDetailState();
  }
}

class MovieDetailState extends State<MovieDetailStateful> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(blackPrimaryValue),
      body: SafeArea(
        child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                elevation: 0.0,
                backgroundColor: Color(blackPrimaryValue),
                expandedHeight: 350.0,
                floating: false,
                pinned: true,
                flexibleSpace: FlexibleSpaceBar(
                  centerTitle: false,
                  title: Text(
                    widget.movieData.originalTitle,
                  ),
                  background: CachedNetworkImage(
                    imageUrl: "$img_url${widget.movieData.posterPath}",
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ];
          },
          body: MovieDetailInfoView(widget.movieData),
        ),
      ),
    );
  }
}
