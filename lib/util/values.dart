

const xSmallPadding = 5.0;
const smallPadding = 10.0;
const mediumPadding = 16.0;
const largePadding = 20.0;

const mediumFontSize = 16.0;
const largeFontSize = 20.0;
const xlargeFontSize = 23.0;

const largeIconSize = 30.0;