
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_movie/util/values.dart';

class ListBottomLoader extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(largePadding),
      alignment: Alignment.center,
      child: Center(
        child: SizedBox(
          width: largePadding,
          height: largePadding,
          child: CircularProgressIndicator(
            strokeWidth: 1,
          ),
        ),
      ),
    );
  }
}