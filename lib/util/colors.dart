import 'package:flutter/material.dart';

const MaterialColor primaryBlack = MaterialColor(
  blackPrimaryValue,
  <int, Color>{
    50: Color(0xFF141A2D),
    100: Color(0xFF141A2D),
    200: Color(0xFF141A2D),
    300: Color(0xFF141A2D),
    400: Color(0xFF141A2D),
    500: Color(blackPrimaryValue),
    600: Color(0xFF141A2D),
    700: Color(0xFF141A2D),
    800: Color(0xFF141A2D),
    900: Color(0xFF141A2D),
  },
);

const int blackPrimaryValue = 0xFF141A2D;