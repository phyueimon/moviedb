
import 'package:flutter_movie/api/movie_data_agent.dart';
import 'package:flutter_movie/api/movie_data_agent_impl.dart';
import 'package:flutter_movie/data_model/popular_movie_response.dart';

class MovieRepository {
  final MovieDataAgent _movieDataAgent = MovieDataAgentImpl();

  Future<PopularMovieResponse> fetchPopularMovie(int page) async{
    return _movieDataAgent.fetchMovie(page+1).then((response) {
      return response;
    });
  }
}