import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter_movie/util/constants.dart';

class ApiGeneral {
  static final ApiGeneral _singleton = ApiGeneral._internal();

  factory ApiGeneral() {
    return _singleton;
  }

  ApiGeneral._internal();

  bool checkIfStatusOk(Response response) {
    if (response.statusCode != 200 && response.data == null) {
      return false;
    } else {
      return true;
    }
  }

  Future<bool> checkConnectivity() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      }
      return false;
    } on SocketException catch (_) {
      return false;
    }
  }

  Future<Response> get(String urlPart) {
    return checkConnectivity().then((conn) {
      if (conn) {
        try{
          Dio dio = Dio();
          dio.interceptors.add(InterceptorsWrapper(
              onRequest:(RequestOptions options) async {
                return options;
              },
              onResponse:(Response response) async {
                return response;
              },
              onError: (DioError e) async {
                return Response(statusCode: e.response.statusCode);
              }
          ));
          return dio.get(base_url + urlPart);
        }on DioError catch(e){
          return Response(statusCode:e.response.statusCode);
        }
      } else {
        return Response(statusCode: 502);
      }
    });
  }
}
