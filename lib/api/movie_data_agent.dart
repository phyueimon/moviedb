
import 'package:flutter_movie/data_model/popular_movie_response.dart';

abstract class MovieDataAgent{
  Future<PopularMovieResponse> fetchMovie(int page);
}