import 'package:flutter_movie/api/api_general.dart';
import 'package:flutter_movie/api/movie_data_agent.dart';
import 'package:flutter_movie/data_model/popular_movie_response.dart';
import 'package:flutter_movie/util/constants.dart';

class MovieDataAgentImpl extends MovieDataAgent {
  static final MovieDataAgentImpl _singleton = MovieDataAgentImpl._internal();
  final apiGeneral = ApiGeneral();
  
  factory MovieDataAgentImpl() {
    return _singleton;
  }

  MovieDataAgentImpl._internal();

  @override
  Future<PopularMovieResponse> fetchMovie(int page) {
    return apiGeneral.get("$popular_movie_url?api_key=$api_key&page=$page").then((response) {
      if(apiGeneral.checkIfStatusOk(response)){
        return PopularMovieResponse.fromJson(response.data,response.statusCode);
      }else{
        return PopularMovieResponse.fromJson(null, response.statusCode);
      }
    });
  }
}
